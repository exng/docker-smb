# Docker Standalone Samba Server

Can be used to quick setup a simple Samba container in bridge network mode. It will run as if on host, so it can be accessed from other computers on the same network.

It also mounts a host directory as the Samba shared point.

## Warning

Note the simple `smb.conf` file.

It will start a **writable Guests allowed server**.

## Running

Create a new container with the image like following:
```
$ docker run -d \
  --name samba-share \
  -p 445:445 \
  -v /data/tmp:/Shared:ro \
  --network=bridge \
  --restart unless-stopped \
  registry.gitlab.com/exng/docker-smb:latest
```

Here the mount will be read only, you can make it writable by removing `:ro` from the `-v` parameter.

## License

The repository is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).

